//    HipparchiaGoServer
//    Copyright: E Gunderson 2022-25
//    License: GNU GENERAL PUBLIC LICENSE 3
//        (see LICENSE in the top level directory of the distribution)

package vv

const (
	MYNAME    = "HipparchiaGoServer"
	SHORTNAME = "HGS"
	VERSION   = "1.3.8"
)
