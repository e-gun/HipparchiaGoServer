//    HipparchiaGoServer
//    Copyright: E Gunderson 2022-25
//    License: GNU GENERAL PUBLIC LICENSE 3
//        (see LICENSE in the top level directory of the distribution)

package vlt

import (
	"encoding/json"
	"fmt"
	"github.com/e-gun/HipparchiaGoServer/internal/base/str"
	"github.com/e-gun/HipparchiaGoServer/internal/lnch"
	"github.com/e-gun/HipparchiaGoServer/internal/vv"
	"io"
	"os"
	"sync"
)

var (
	UserPassPairs = make(map[string]string)
)

// BuildUserPassPairs - set up authentication map via CONFIGAUTH
func BuildUserPassPairs(cc str.CurrentConfiguration) {
	const (
		FAIL1 = `BuildUserPassPairs() failed to unmarshall authorization config file`
		FAIL2 = `You are requiring authentication but there are no UserPassPairs: aborting vv`
		FAIL3 = "Could not open '%s'"
	)

	uh, _ := os.UserHomeDir()
	h := fmt.Sprintf(vv.CONFIGALTAPTH, uh)
	pwf := fmt.Sprintf("%s%s", h, vv.CONFIGAUTH)

	pwc, e := os.Open(pwf)
	if e != nil {
		Msg.CRIT(fmt.Sprintf(FAIL3, pwf))
	}
	defer func(pwc *os.File) {
		err := pwc.Close()
		if err != nil {
		} // the file was almost certainly not found in the first place...
	}(pwc)

	filebytes, _ := io.ReadAll(pwc)

	type UserPass struct {
		User string
		Pass string
	}

	var upp []UserPass
	err := json.Unmarshal(filebytes, &upp)
	if err != nil {
		Msg.NOTE(FAIL1)
	}

	for _, u := range upp {
		UserPassPairs[u.User] = u.Pass
	}

	if cc.Authenticate && len(UserPassPairs) == 0 {
		Msg.CRIT(FAIL2)
		Msg.ExitOrHang(1)
	}
}

//
// THREAD SAFE INFRASTRUCTURE: MUTEX
//

// makeauthorizedvault - called only once; yields the AllAuthorized vault
func makeauthorizedvault() authvault {
	return authvault{
		UserMap: make(map[string]bool),
		mutex:   sync.RWMutex{},
	}
}

// authvault - there should be only one of these; and it contains all the authorization info
type authvault struct {
	UserMap map[string]bool
	mutex   sync.RWMutex
}

func (av *authvault) Check(u string) bool {
	if !lnch.Config.Authenticate {
		return true
	}
	av.mutex.Lock()
	defer av.mutex.Unlock()
	s, e := av.UserMap[u]
	if e != true {
		av.UserMap[u] = false
		s = false
	}
	return s
}

func (av *authvault) Register(u string, b bool) {
	av.mutex.Lock()
	defer av.mutex.Unlock()
	av.UserMap[u] = b
	return
}
